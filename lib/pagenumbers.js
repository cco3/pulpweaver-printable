import chalk from 'chalk'
import { promises as fs } from 'fs'
import glob from 'globby'
import pdfjs from 'pdfjs-dist/legacy/build/pdf.js'
import path from 'path'
import { parser as synctex } from '@inkylabs/synctex-js'
import { promisify } from 'util'
import zliba from 'zlib'

const red = m => console.error(chalk.red(m))

const zlib = {
  unzip: promisify(zliba.unzip)
}

async function readSyncTex (filepath) {
  filepath = filepath || 'main.synctex.gz'
  const data = await fs.readFile(filepath)
  const buffer = await zlib.unzip(data)
  return synctex.parseSyncTex(buffer.toString())
}

function getElements (st, p, inputLines) {
  const blocks = [...st.pages[p].blocks]
  const elements = []
  while (blocks.length) {
    const b = blocks.pop()
    blocks.push(...b.blocks)

    elements.push(...b.elements
      .map(e => Object.assign({
        y: e.bottom + st.offset.y,
        eb: e.bottom,
        soff: st.offset.y
      }, e)))
  }
  return elements
    .filter(e => e.fileNumber !== 1 || !inputLines.includes(e.line))
}

function assumeBibliography (pinfo) {
  // synctex doesn't seem to pick up on the bibliography.
  // If there's no file name, we'll just assume that's the reason why until we
  // can find a better solution.
  if (pinfo.file !== '_') return
  pinfo.file = '../bibliography.tex'
}

function addType (pinfo) {
  const m = pinfo.file.match(/\.\.\/(.*?)(\.tex)?$/)
  if (m) {
    const key = m[1].replace(/-/g, '_')
    pinfo[`type_${key}`] = true
    pinfo.type = key
  }
}

function addTocLines (pinfo, tocLines) {
  if (pinfo.file === '../main.toc') {
    pinfo.tocentry = tocLines[pinfo.line]
    pinfo.type_toc = true
    pinfo.type = 'toc'
  }
}

const _indexNums = {}
function addIndex (pinfo) {
  if (!pinfo.type) return
  const res = [
    /(.*).ind$/,
    /^index_(.*)/
  ]
  for (const re of res) {
    const m = pinfo.type.match(re)
    if (!m) continue
    const key = m[1]
    pinfo.type = 'index'
    pinfo.index_key = key
    if (!(key in _indexNums)) {
      _indexNums[key] = Object.keys(_indexNums).length
    }
    pinfo.index_number = _indexNums[key]
  }
}

function addPdfInfo (pinfo, pdfPages) {
  const info = pdfPages[pinfo.page]
  pinfo.label = info.label
  for (const item of info.text.items) {
    // Find text at a matching vertical position.
    const y = info.view[3] - item.transform[5]
    const diff = pinfo.y - y
    if (Math.abs(diff) < 0.01) {
      pinfo.text = item.str
      return
    }
  }
}

// Code from remark-pages I'm not ready to throw away.
// const pageText = page.text
//  // We can't normalize this to an ellipsis because we wouldn't know
//  // whether four dots should be "…." or ".…".
//  .replace('...', '.')
// for (let i = 0; i < pageText.length; i++) {
//  const ss = pageText.substring(0, i + 1)
//  const nodeText = node.value
//    .replace(/\s+/g, ' ')
//    // Accommodate OCR by normalizing characters.
//    // We can't use three dots because it has to be the same number of
//    // characters.
//    .replace(/…/g, '.')
//  const idx = nodeText.indexOf(ss)
function getTexLines (st, inputLines, beginLines, tocLines, pdfPages) {
  const texLines = []
  for (let p = 1; p <= st.numberPages; p++) {
    // cure is the current element.
    let cure = {
      line: Number.MAX_SAFE_INTEGER,
      fileNumber: Number.MAX_SAFE_INTEGER
    }
    for (const e of getElements(st, p, inputLines)) {
      if (e.fileNumber > cure.fileNumber) continue
      if (e.fileNumber === cure.fileNumber && e.line > cure.line) continue

      cure = e
    }
    if (!cure.y) {
      cure.line = 0
      cure.file = {
        path: 'genfiles/_',
        name: '_'
      }
    }

    const pinfo = {
      page: p,
      file: path.relative('genfiles', cure.file.path),
      line: cure.line,
      y: cure.y
    }
    addPdfInfo(pinfo, pdfPages)
    assumeBibliography(pinfo, tocLines)
    addType(pinfo)
    addTocLines(pinfo, tocLines)
    addIndex(pinfo)
    texLines.push(pinfo)

    if (pinfo.file === '../main.tex') {
      red(`Page ${pinfo.label} starts directly from main.tex on ` +
        `line ${pinfo.line}. This shouldn't happen`)
    }
  }
  return texLines
}

// a is the string that starts first.
function commonCharacters (a, b) {
  if (!a || !b) return ''
  let i
  for (i = 1; i < b.length + 1; i++) {
    if (!a.includes(b.substring(0, i))) {
      break
    }
  }
  return b.substring(0, i - 1)
}

function getFirstPageElem (pinfo, root) {
  if (!root.texStartLine || root.texStartLine > pinfo.line) return null

  // The lines should overlap.
  if ([
    'footnote',
    'indexend',
    'indexfmt',
    'indexstart'
  ].includes(root.type)) return null
  if ([
    'heading',
    'subheading'
  ].includes(root.type)) return root

  let lastNode
  for (const c of root.children || []) {
    const node = getFirstPageElem(pinfo, c)
    if (node) lastNode = node
  }
  if (lastNode &&
    (lastNode.type === 'text' ||
      root.type === 'root' ||
      lastNode.position.start.line !== root.position.start.line ||
      lastNode.position.start.column !== root.position.start.column)) {
    return lastNode
  }

  if (root.type === 'text') {
    root.commonCharacters = commonCharacters(root.value || '', pinfo.text)
    if (root.commonCharacters === '') return null
    root.commonCharactersStart = root.value.indexOf(root.commonCharacters)
    const firstHalf = root.value.substring(0, root.commonCharactersStart)
    let l1 = root.position.start.line
    let l2 = root.position.start.line
    let c1 = root.position.start.column
    let c2 = root.position.start.column
    for (const c of firstHalf) {
      if (c === '\n') {
        l2++
        c2 = 1
      } else {
        l1 = l2
        c1 = c2
        c2++
      }
    }
    root.position = Object.assign({
      middle1: {
        line: l1,
        column: c1,
        offset: root.position.start.offset + root.commonCharactersStart - 1
      },
      middle2: {
        line: l2,
        column: c2,
        offset: root.position.start.offset + root.commonCharactersStart
      }
    }, root.position)
  }
  return root
}

/**
 * getMdLines gets the coordinates of the markdown element that triggers a page
 * break.
 */
function getMdLines (roots, texLines) {
  const mdLines = []
  texLines.forEach(pinfo => {
    const mdf = pinfo.file.replace(/.tex$/, '')
    const mdl = Object.assign(pinfo, {
      file: mdf
    })
    mdLines.push(mdl)
    const root = roots[mdf]
    if (!root) return
    let node = getFirstPageElem(pinfo, root)
    if (!node) {
      console.error('Could not find first page element', pinfo, root)
      node = root
    }
    mdl.type = node.type
    mdl.commonCharacters = node.commonCharacters
    mdl.commonCharactersStart = node.commonCharactersStart
    mdl.position = node.position
  })
  return mdLines
}

async function getInputLines (filepath) {
  const tex = await fs.readFile(filepath, 'utf8')
  return tex.split('\n')
    .map((line, i) => line.match(/^ *(\\input\{.*\}|\\end\{document\})$/) ? i + 1 : 0)
    .filter(i => !!i)
}

async function getTocLines (filepath) {
  let tex
  try {
    tex = await fs.readFile(filepath, 'utf8')
  } catch (e) {
    if (e.code !== 'ENOENT') throw e
    return {}
  }
  return Object.fromEntries(tex.split('\n')
    .map((line, i) => line.match(/^\\contentsline/) ? i + 1 : 0)
    .filter(i => !!i)
    .map((i, j) => [i, j]))
}

// This maps lines after \begin to the begin that comes before.
// Example:
// genfiles/chapter/1.tex
// 1 \begin{outer}
// 2 \begin{inner}
// 3 content
// 4 \end{inner}
// 5 \end{outer}
// 6 more content
// returns {'1.tex': {3: 1}}
async function getBeginLines (dirpath) {
  const beginLines = {}
  const files = await glob(path.join(dirpath, '**', '*.tex'))
  for (const f of files) {
    const fpath = path.relative('genfiles', f)
    beginLines[fpath] = {}
    const data = await fs.readFile(f, 'utf8')
    let begin
    data.split('\n').forEach((l, i) => {
      i = i + 1 // page numbers are 1-indexed
      const isBegin = !!l.match(/^ *\\begin\{.*\}/)
      const isEmpty = !!l.match(/^ *(%.*)?$/)
      const isContent = !isBegin && !isEmpty
      if (begin) {
        if (isContent) {
          beginLines[fpath][i] = begin
          begin = null
        }
      } else if (isBegin) {
        begin = i
      }
    })
  }
  return beginLines
}

async function getPdfPages (filepath) {
  const buffer = await fs.readFile(filepath)
  const doc = await pdfjs.getDocument({
    data: buffer
  }).promise
  const labels = await doc.getPageLabels()
  if (!labels) return null
  const pages = {}
  for (let i = 1; i <= doc.numPages; i++) {
    const page = await doc.getPage(i)
    const text = await page.getTextContent()
    pages[i] = {
      page,
      text,
      view: page.view,
      label: labels[i - 1]
    }
  }
  return pages
}

async function process (roots, filepath) {
  const st = await readSyncTex(filepath)
  const inputLines = await getInputLines('./main.tex')
  const tocLines = await getTocLines('./main.toc')
  const beginLines = await getBeginLines('genfiles')
  const pdfPages = await getPdfPages('./main.pdf')
  if (!pdfPages) {
    console.error('Could not get page labels. Giving up.')
    return
  }
  const texLines = getTexLines(st, inputLines, beginLines, tocLines, pdfPages)
  const mdLines = getMdLines(roots, texLines)
  await fs.writeFile('pages.json', JSON.stringify(mdLines, null, 2))
}

export default {
  process
}
