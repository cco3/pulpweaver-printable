\documentclass[{{fontsize}},{{papersize}}]{memoir}
\synctex=1

\usepackage{changepage}
\usepackage{amsthm}
\usepackage[american]{babel}
\usepackage[style=authortitle-ibid,backend=biber,bibencoding=utf8]{biblatex}
\usepackage{csquotes}
\usepackage{ebgaramond}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[protrusion=true,expansion=true]{microtype}
%\usepackage[indentunit=3ex]{idxlayout}
\usepackage{imakeidx}
\usepackage{suffix}
\usepackage{textgreek}
\usepackage{wrapfig}
\usepackage{yfonts}
\usepackage{xcolor}

% Margins %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlrmarginsandblock{ {{~margin.spine~}} }{ {{~margin.edge~}} }{*}
\setulmarginsandblock{ {{~margin.upper~}} }{ {{~margin.lower~}} }{*}
\checkandfixthelayout

% Text %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareUnicodeCharacter{200B}\{{~\hskip 0pt~}}
\urlstyle{same}
{{#each hyphenations}}
\hyphenation{ {{~this~}} }
{{/each}}

% Headers
\title{The Dorean Principle}
\nouppercaseheads
\makepagestyle{mainstyle}
\makeevenhead{mainstyle}{\thepage}{}{\itshape \thetitle}
\makeoddhead{mainstyle}{\itshape \leftmark}{}{\thepage}
\makeevenfoot{mainstyle}{}{}{}
\makeoddfoot{mainstyle}{}{}{}
% This makes \leftmark only produce the chapter title.
\makepsmarks{mainstyle}{\createmark{chapter}{left}{nonumber}{}{}}
\makepagestyle{bstyle}
\makeevenhead{bstyle}{\thepage}{h}{\itshape The Dorean Principle}
\makeoddhead{bstyle}{\itshape \leftmark}{}{\thepage}
\makeevenfoot{bstyle}{d}{e}{f}
\makeoddfoot{bstyle}{a}{b}{c}
% This makes \leftmark only produce the chapter title.
\makepsmarks{bstyle}{\createmark{chapter}{left}{nonumber}{}{}}

% Bibliography
{{#each bibfiles}}
\addbibresource{sources.bib}
{{/each}}
\renewcommand*{\bibfont}{\small}
\toks0\expandafter{\biburlsetup}
\edef\biburlsetup{\the\toks0 \Urlmuskip =0mu\relax}
% Remove the pagination prefix.
\DeclareFieldFormat{postnote}{#1}

% Indicies
% https://tex.stackexchange.com/questions/401712/how-do-i-format-a-scripture-index-manually-like-the-one-pictured
% https://tex.stackexchange.com/questions/354752/how-to-use-different-formatting-for-multiple-indices-with-imakeidx-and-ist-file/354996#354996
% These two commands eat spaces before and after our command as needed, and
% also ensure chktex doesn't yell at us for having spaces before the command.
\newcommand\indexbegin[2][main]{\index[#1]{#2|(}\ignorespaces}
\newcommand\indexend[2][main]{\leavevmode\unskip\index[#1]{#2|)}}
\indexsetup{othercode=\small}
\def\dotfill{%
  \leavevmode
  \cleaders \hbox to .44em{\hss.\hss}\hskip 1em plus1fill
  \kern 0pt}

{{#each indices}}
\makeindex{{{spec}}}
{{/each}}

%% Chapter titles
%\renewcommand{\contentsname}{\vspace{-1cm}Contents}
%\patchcmd{\chapter}{\cleardoublepage}{\clearpage}{}{}
%\renewcommand\cftchapafterpnum{\vspace{-2ex}}
% See these commands and associated commands in the memoir docs.
\renewcommand{\printchaptername}{\chapnamefont}
\renewcommand{\chapnamefont}{\centering\normalfont\huge\bfseries\color{gray}}
\renewcommand{\chaptitlefont}{\centering\normalfont\Huge\bfseries\color{black}}

\let\oldprechapterprecis\prechapterprecis
\let\oldpostchapterprecis\postchapterprecis
\renewcommand{\prechapterprecis}{\oldprechapterprecis\begin{center}}
\renewcommand{\postchapterprecis}{%
\end{center}\vspace{5ex}\oldpostchapterprecis%
}
\prechapterprecisshift=-2\baselineskip
\renewcommand{\precisfont}{\Large\itshape\bfseries}

\newcommand\Chapter[1]{\chapter{#1}}
\WithSuffix\newcommand\Chapter*[1]{%
\chapter*{#1}\addcontentsline{toc}{chapter}{#1}%
}

% Table of Contents
\renewcommand*{\cftappendixname}{\appendixname\space}
\renewcommand*{\cftchapterfont}{\normalfont}
\renewcommand*{\cftchapterpagefont}{\normalfont}
\renewcommand*{\cftchapterleader}{%
  \cftchapterfont\cftdotfill{\cftchapterdotsep}}
\renewcommand*{\cftchapterdotsep}{\cftdotsep}

\newtheoremstyle{plain}{}{}{\itshape}{}{\bfseries}{:}{.5em}{\thmnote{#3}}
\theoremstyle{plain}
\newtheorem*{definition}{}

% Lists
% Could use \tightlists for even less space between items.
\firmlists

% This command is a hack to ensure the first line of the pdf is attributed to
% certain input files for page numbering purposes.
\newcommand{\zerobox}{\makebox(0,0){}}

{{#if preamble}}
\input{genfiles/{{preamble}}.tex}
{{/if}}

\begin{document}

\frontmatter

{{#if endorsements}}
\pagestyle{empty}
\input{endorsements-before.tex}
\input{genfiles/{{endorsements}}.tex}
\input{endorsements-after.tex}
{{/if}}

{{#if halftitle}}
\pagestyle{empty}
\input{halftitle-before.tex}
\input{genfiles/{{halftitlepage}}.tex}
\input{halftitle-after.tex}
{{/if}}

\pagestyle{mainstyle}
{{#if copyright}}
\titlingpageend{\clearpage}{\clearpage}
{{/if}}
{{#if titletex}}
\input{title-before.tex}
\input{../../{{titletex~}} }
\input{title-after.tex}
{{/if}}
{{#if titlepage}}
\input{title-before.tex}
\input{genfiles/{{titlepage}}.tex}
\input{title-after.tex}
{{/if}}

{{#each frontmatter}}
\input{ {{~this~}} }
{{/each}}

{{#if copyright}}
\input{copyright-before.tex}
\input{genfiles/{{copyright}}.tex}
\input{copyright-after.tex}
{{/if}}

{{#if dedication}}
\input{dedication-before.tex}
\input{genfiles/{{dedication}}.tex}
\input{dedication-after.tex}
{{/if}}

{{#if tableofcontents}}
\begin{KeepFromToc}
  \tableofcontents
  \input{toc-after.tex}
\end{KeepFromToc}
{{/if}}

{{#if listoffigures}}
\begin{KeepFromToc}
  \input{listoffigures-before.tex}
  \input{listoffigures.tex}
  \input{listoffigures-after.tex}
\end{KeepFromToc}
{{/if}}

{{#if foreword}}
\input{foreword-before.tex}
\input{genfiles/{{foreword}}.tex}
\input{foreword-after.tex}
{{#if tableofcontents}}
\addtocontents{toc}{\vskip\cftbeforepartskip}
{{/if}}
{{/if}}

% BEGIN mainmatter
\mainmatter
{{#each chapters}}
\input{genfiles/{{this}}.tex}
{{/each}}

% BEGIN appendix
\appendix
\makeatletter
\renewcommand{\printchaptername}{\chapnamefont \@chapapp}
\makeatother
% Add some space in the table of contents between the main chapters and other
% things.
{{#if tableofcontents}}
\addtocontents{toc}{\vskip\cftbeforepartskip}
{{/if}}
{{#each appendices}}
\input{genfiles/{{this}}.tex}
{{/each}}
{{#if tableofcontents}}
\addtocontents{toc}{\vskip\cftbeforepartskip}
{{/if}}

{{#if bibliography}}
\begingroup
\raggedright
\input{bibliography.tex}
\endgroup
{{/if}}

{{#each indices}}
  \input{index_{{key}}.tex}
{{/each}}
\end{document}
