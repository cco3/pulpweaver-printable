import { toText } from '@inkylabs/remark-utils'
import chalk from 'chalk'
import childProcess from 'child_process'
import esDirname from 'es-dirname'
import glob from 'globby'
import { promises as fs } from 'fs'
import handlebars from 'handlebars'
import path from 'path'

import pagenumbers from './lib/pagenumbers.js'

const __dirname = esDirname()
const NAME = 'printable'

const isString = s => typeof s === 'string' || s instanceof String

function wrapCmd (node, ctx, cmd, opts) {
  opts = Object.assign({
    options: null
  }, opts)
  let options = ''
  if (opts.options) {
    const joined = opts.options.join(',')
    options = `[${joined}]`
  }
  ctx.output += `\\${cmd}${options}{`
  ctx.output += '%\n'
  handleBlock(node, ctx)
  ctx.output += '}'
}

function genEnvBegin (env, opts) {
  opts = Object.assign({
    options: null,
    args: []
  }, opts)
  let options = ''
  if (opts.options) {
    const joined = opts.options.join(',')
    options = `[${joined}]`
  }
  const args = opts.args
    .map(a => `{${a}}`)
    .join('')
  return `\\begin{${env}}${args}${options}`
}

function wrapEnvBlock (node, ctx, env, opts) {
  const a = node.attributes
  if (a && a.args) {
    opts = Object.assign({
      args: a.args.split('%')
    }, opts)
  }
  ctx.output += genEnvBegin(env, opts) + '\n'
  handleBlock(node, ctx, { wrapped: true })
  ctx.output += `\\end{${env}}\n`
}

function handleNode (node, ctx) {
  const origLength = ctx.output.length
  let isolate = true

  switch (node.type) {
    case 'blockquote':
      handleBlockquote(node, ctx)
      break
    case 'caption':
      // The caption is handled in handleFigure.
      break
    case 'Cite':
    case 'cite':
      handleCite(node, ctx)
      break
    case 'comment':
      break
    case 'controlspace':
      isolate = false
      ctx.output += '\\ '
      break
    case 'definition':
      handleDefinition(node, ctx)
      break
    case 'emphasis':
      wrapCmd(node, ctx, 'textit')
      break
    case 'figure':
      handleFigure(node, ctx)
      break
    case 'fraktur':
      wrapCmd(node, ctx, 'textfrak')
      break
    case 'footnote':
      handleFootnote(node, ctx)
      break
    case 'hang':
      handleHang(node, ctx)
      break
    case 'heading':
      handleHeading(node, ctx)
      break
    case 'html':
      break
    case 'image':
      handleImage(node, ctx)
      isolate = false
      break
    case 'indexfmt':
      handleIndexFmt(node, ctx)
      break
    case 'indexstart':
    case 'indexend':
      handleIndex(node, ctx)
      break
    case 'label':
      handleLabel(node, ctx)
      break
    case 'linebreak':
      handleLinebreak(node, ctx)
      break
    case 'link':
      handleLink(node, ctx)
      break
    case 'list':
      handleList(node, ctx)
      break
    case 'listItem':
      handleListItem(node, ctx)
      break
    case 'medspace':
      ctx.output += '\\hspace*{3ex}'
      break
    case 'noindent':
      ctx.output += '\\noindent'
      break
    case 'paragraph':
      handleParagraph(node, ctx)
      isolate = false
      break
    case 'attribution':
      handleAttribution(node, ctx)
      break
    case 'ref':
      ctx.output += `\\ref{${node.value}}`
      break
    case 'root':
      handleBlock(node, ctx)
      ctx.output += '\n'
      break
    case 'strong':
      wrapCmd(node, ctx, 'textbf')
      break
    case 'smallcaps':
      wrapCmd(node, ctx, 'textsc')
      break
    case 'softhyphen':
      ctx.output += '\\-'
      break
    case 'span':
      handleBlock(node, ctx)
      break
    case 'subheading':
      handleSubheading(node, ctx)
      break
    case 'subscript':
      wrapCmd(node, ctx, 'textsubscript')
      break
    case 'superscript':
      wrapCmd(node, ctx, 'textsuperscript')
      break
    case 'table':
      handleTable(node, ctx)
      break
    case 'tableBody':
      handleBlock(node, ctx)
      break
    case 'tableCell':
      handleTableCell(node, ctx)
      break
    case 'tableHead':
      handleBlock(node, ctx)
      break
    case 'tableRow':
      handleTableRow(node, ctx)
      break
    case 'tex':
      ctx.output += node.value
      break
    case 'texenv':
      wrapEnvBlock(node, ctx, node.name)
      break
    case 'text':
      handleText(node, ctx)
      break
    case 'underline':
      wrapCmd(node, ctx, 'underline')
      break
    default:
      ctx.file.message(`unknown type: ${node.type}`, node.position,
        `${NAME}:no-unknown-type`)
  }

  node.texStartLine = (ctx.output.substr(0, origLength + 1) + '\n')
    .match(/\n/g)
    .length
  node.texEndLine = (ctx.output + '\n')
    .match(/\n/g)
    .length
  if (isolate) ctx.output += `%${node.type}\n`
}

function handleBlock (node, ctx, opts) {
  opts = Object.assign({
    wrapped: false
  }, opts)
  const cl = (node.attributes && node.attributes.class) || ''
  const mods = []
  if (cl.includes('tiny')) mods.push('\\tiny ')
  if (cl.includes('scriptsize')) mods.push('\\scriptsize ')
  if (cl.includes('footnotesize')) mods.push('\\footnotesize ')
  if (cl.includes('small')) mods.push('\\small ')
  if (cl.includes('normalsize')) mods.push('\\normalsize ')
  if (cl.includes('large')) mods.push('\\large ')
  if (cl.includes('Large')) mods.push('\\Large ')
  if (cl.includes('LARGE')) mods.push('\\LARGE ')
  if (cl.includes('huge')) mods.push('\\huge ')
  if (cl.includes('Huge')) mods.push('\\Huge ')
  if (cl.includes('it')) mods.push('\\itshape{}')
  const curly = mods.length && !opts.wrap
  if (curly) ctx.output += '{'
  ctx.output += mods.join('')
  ctx.blockStart = true
  for (const c of node.children) {
    handleNode(c, ctx)
    ctx.blockStart = false
  }
  ctx.blockStart = false
  if (curly) ctx.output += '}'
}

function handleAttribution (node, ctx) {
  if (ctx.quoting) ctx.output += '”\n'
  ctx.quoting = false
  ctx.output += '\\begin{adjustwidth}{2\\absparindent}{}\n'
  ctx.output += '\\noindent\\llap{—}%\n'
  wrapCmd(node.labelNode, ctx, 'textbf')
  if (node.contentsNode.children.length) {
    ctx.output += ', '
    handleBlock(node.contentsNode, ctx)
  }
  ctx.output += '\\end{adjustwidth}\n'
}

function handleBlockquote (node, ctx) {
  const fw = node.attributes.class.includes('fullwidth')
  const npb = node.attributes.class.includes('nopagebreak')
  if (!ctx.blockStart) ctx.output += '\n\n\n'
  if (npb) ctx.output += '\\noindent\\begin{minipage}{\\textwidth}\n'
  if (fw) {
    ctx.output += '\\vspace{\\parsep}\n'
    ctx.output += '\\noindent\\llap{“}%\n'
    ctx.quoting = true
    handleBlock(node, ctx)
    if (ctx.quoting) ctx.output += '”'
    ctx.quoting = false
  } else {
    wrapEnvBlock(node, ctx, 'quote')
  }
  if (npb) ctx.output += '\\end{minipage}\n\n'
  if (fw) ctx.output += '\\vspace{\\parsep}\n\n'
}

function handleCite (node, ctx) {
  if (node.nohyper) ctx.output += '\\begin{NoHyper}'
  wrapCmd(node, ctx, node.type, {
    options: [
      node.pages
    ]
  })
  if (node.nohyper) ctx.output += '\\end{NoHyper}'
}

function handleDefinition (node, ctx) {
  ctx.output += '\\begin{definition}['
  handleBlock(node.labelNode, ctx)
  ctx.output += ']\n'
  handleBlock(node.contentsNode, ctx)
  ctx.output += '\\end{definition}'
}

function handleFootnote (node, ctx) {
  wrapCmd(node, ctx, 'footnote')
}

function handleHang (node, ctx) {
  ctx.output += '\\noindent'
  wrapCmd(node, ctx, 'llap')
}

function handleHeading (node, ctx) {
  if (node.depth > 1) ctx.output += '\n\n'
  const attrs = node.attributes
  const star = attrs['-'] ? '*' : ''
  switch (node.depth) {
    case 1:
      ctx.depth = 1
      wrapCmd(node, ctx, `Chapter${star}`)
      break
    case 2:
      ctx.depth = 2
      wrapCmd(node, ctx, `section${star}`)
      break
    case 3:
      ctx.depth = 3
      wrapCmd(node, ctx, `subsection${star}`)
      break
    case 4:
      ctx.depth = 4
      wrapCmd(node, ctx, `subsubsection${star}`)
      break
    default:
      ctx.file.message(`unhandled heading depth: ${node.depth}`,
        node.position, `${NAME}:known-depth`)
  }
  ctx.output += '%\n'
}

function handleFigure (node, ctx) {
  ctx.output += ctx.blockStart ? '' : '\n\n'
  ctx.output += genEnvBegin('figure', {
    options: node.figpos ? [node.figpos] : undefined
  })
  ctx.output += '%\n'
  ctx.output += '\\zerobox{}%\n'
  handleBlock(node, ctx)

  const captions = node.children.filter(c => c.type === 'caption')
  if (captions.length) {
    wrapCmd(captions[0], ctx, 'caption')
    ctx.output += '\n'
  }

  ctx.output += '\\end{figure}'
}

function handleImage (node, ctx) {
  const imgpath = path.join('..', '..', node.url)
  const width = node.attributes.width || '\\textwidth'
  ctx.output += `\\includegraphics[width=${width}]{${imgpath}}\n`
}

function handleIndexFmt (node, ctx) {
  const a = node.attributes
  const idx = node.indexKey ? `[${node.indexKey}]` : ''
  if (a.see) {
    ctx.output += `\\index${idx}{${node.key}@|see{${a.see}}}`
  }
  if (a.seealso) {
    ctx.output += `\\index${idx}{${node.key}@|seealso{${a.seealso}}}`
  }
}

function handleIndex (node, ctx) {
  const cmd = node.type === 'indexstart' ? 'begin' : 'end'
  const ik = node.indexKey
  const gk = node.groupKey
  const gn = node.groupName
  const ek = node.entryKey
  const en = node.entryName
  const index = ik ? `[${ik}]` : ''
  const group = gk ? `${gk}@${gn}!` : ''
  ctx.output += `\\index${cmd}${index}{${group}${ek}@`
  if (en.children) {
    handleBlock(en, ctx)
  } else if (en.value) {
    ctx.output += en.value
  } else {
    ctx.ouptut += en
  }
  ctx.output += '}'
}

function handleLabel (node, ctx) {
  ctx.output += `\\label{${node.value}}`
}

function handleLinebreak (node, ctx) {
  ctx.output += '\\\\'
  if (node.attributes.class.includes('nopagebreak')) ctx.output += '*'
}

function handleLink (node, ctx) {
  const url = toText(node)
  ctx.output += `\\url{${url}}%\n`
}

function handleList (node, ctx) {
  const env = node.ordered ? 'enumerate' : 'itemize'
  ctx.output += ctx.blockStart ? '\n' : '\n\n'
  ctx.output += `\\begin{${env}}\n`
  if (node.start !== 1 && node.start !== null) {
    ctx.output += `\\setcounter{enumi}{${node.start - 1}}\n`
  }
  handleBlock(node, ctx)
  ctx.output += `\n\\end{${env}}`
}

function handleListItem (node, ctx) {
  ctx.output += '  '
  wrapCmd(node, ctx, 'item')
  ctx.output += '\n'
}

function handleParagraph (node, ctx) {
  ctx.output += ctx.blockStart ? '' : '\n\n'
  handleBlock(node, ctx)
}

function handleSubheading (node, ctx) {
  wrapCmd(node, ctx, 'chapterprecis')
  ctx.output += '%\n'
}

function handleTable (node, ctx) {
  const align = node.align.map(a => {
    switch (a) {
      case 'center':
        return 'c'
      case null:
      case 'left':
        return 'l'
      case 'right':
        return 'r'
      default:
        ctx.file.message(`table alignment ${a} not handled`, node.position,
          `${NAME}:table-alignment-known`)
    }
    return 'l'
  })
    .join('')
  const centered = node.attributes.class.includes('centered')
  if (centered) ctx.output += '\n\\begin{center}\n'
  wrapEnvBlock(node, ctx, 'tabular', {
    args: [align]
  })
  if (centered) ctx.output += '\\end{center}\n'
}

function handleTableCell (node, ctx) {
  if (node.header) {
    wrapCmd(node, ctx, 'textbf')
  } else {
    handleBlock(node, ctx)
  }
}

function handleTableRow (node, ctx) {
  node.children.forEach((c, i) => {
    handleNode(c, ctx)
    ctx.output += i === node.children.length - 1 ? ' \\\\\n' : ' & '
  })
}

function handleText (node, ctx) {
  ctx.output += node.value
    // On a newline, opening spaces will get ignored, but we can bind it with
    // zws.
    // .replace(/^(\s)/, '\u200B$1')
    .replace(/([%${}&])/g, '\\$1') // Special characters
    .replace(/\u00A0/g, '~') // nbsp
    .replace(/\u00AD/g, '\\-') // soft hyphens
    .replace(/^(\s)/, '{$1}')
}

async function linkResources ({ bibfiles }, { indices, resources }) {
  resources = [
    ...bibfiles,
    ...resources,
    ...Object.values(indices)
      .filter(i => i.style)
      .map(i => `${i.style}.ist`)
  ]

  for (const r of resources) {
    try {
      await fs.mkdir(path.dirname(r), { recursive: true })
      await fs.symlink(path.join('..', '..', r), r)
    } catch (e) {
      if (e.code !== 'EEXIST') throw e
    }
  }
}

async function collectFrontmatter (items) {
  const inputs = []
  for (const i of (items || [])) {
    if (isString(i)) {
      const fps = await glob(path.join('..', '..', i))
      inputs.push(...fps)
    }
  }
  return inputs
}

async function getHyphenations (data) {
  if (isString(data)) {
    const d = await fs.readFile(path.join('..', '..', data), 'utf8')
    return d.split(/\n/g).filter(l => !!l)
  }
  return data
}

async function writeMain ({
  appendices,
  bibfiles,
  bibliography,
  chapters,
  copyright,
  dedication,
  endorsements,
  foreword,
  halftitlepage,
  indices,
  listoffigures,
  preamble,
  tableofcontents,
  titlepage
}, {
  frontmatter,
  fontsize,
  halftitle,
  hyphenations,
  indices: indicesConfig,
  margin,
  papersize,
  titletex
}) {
  hyphenations = await getHyphenations(hyphenations)
  frontmatter = await collectFrontmatter(frontmatter)

  indices = indices.map(i => {
    const c = indicesConfig[i.key]
    const spec = [
      i.key ? `name=${i.key}` : null,
      i.title ? `title=${i.title}` : null,
      c.columns ? `columns=${c.columns}` : null,
      c.style ? `options=-s ${c.style}` : null,
      c.intoc ? 'intoc' : null
    ]
      .filter(v => !!v)
      .join(',')
    return {
      key: i.key,
      bracketedKey: i.key ? `[${i.key}]` : '',
      spec: spec ? `[${spec}]` : '',
      title: i.title
    }
  })

  const data = {
    appendices,
    bibfiles,
    bibliography,
    chapters,
    copyright,
    dedication,
    endorsements,
    fontsize,
    foreword,
    frontmatter,
    halftitle,
    halftitlepage,
    hyphenations,
    indices,
    listoffigures,
    margin,
    papersize,
    preamble,
    tableofcontents,
    titlepage,
    titletex
  }
  const templateDir = path.join(__dirname, 'templates')
  const readTemplate = async (name) => {
    const templatePath = path.join(templateDir, `${name}.tex.handlebars`)
    const templateText = await fs.readFile(templatePath, 'utf8')
    return handlebars.compile(templateText)
  }
  const write = async (name) => {
    const template = await readTemplate(name)
    const content = template(data)
    await fs.writeFile(`${name}.tex`, content)
  }

  await write('main')
  await write('halftitle-before')
  await write('halftitle-after')
  await write('title-before')
  await write('title-after')
  await write('copyright-before')
  await write('copyright-after')
  await write('dedication-before')
  await write('dedication-after')
  await write('endorsements-before')
  await write('endorsements-after')
  await write('foreword-before')
  await write('foreword-after')
  await write('toc-after')
  await write('listoffigures-before')
  await write('listoffigures')
  await write('listoffigures-after')
  await write('bibliography')

  const template = await readTemplate('index')
  for (const i of indices) {
    const content = template(i)
    await fs.writeFile(`index_${i.key}.tex`, content)
  }
}

async function runChktex (files) {
  if (!files.length) return 0
  const proc = childProcess.spawn('chktex', [
    '--quiet',
    // Because of some of the line number hacks we do, we frequently end up
    // with `{ }` at the beginning of a line. If the line begins with a
    // parenthesis warning 36 tells us we should have a space before it, even
    // though we do.
    '--nowarn', '36',
    ...files
      .map(c => path.join('genfiles', `${c}.tex`))
  ])
  proc.stdout.on('data', data => {
    const d = data.toString().trim()
    console.log(chalk.red(d))
  })
  proc.stderr.on('data', data => {
    const d = data.toString().trim()
    console.error(chalk.red(d))
  })
  return await new Promise(resolve => proc.on('exit', resolve))
}

async function runLatex (output) {
  const proc = childProcess.spawn('latexmk', [
    '-pdf',
    '-halt-on-error',
    '-norc',
    '-synctex=1',
    'main'
  ])
  let p = 0
  const setPage = s => {
    const m = s.match(/^(?:[[\]0-9]*)\[(\d+)\]?\[?$/)
    if (!m) return false
    p = parseInt(m[1])
    return true
  }
  const ps = s => {
    const ret = p ? `p${p} ${s}` : s
    p = 0
    return ret
  }
  // TODO: Combine data processing functions.
  proc.stdout.on('data', data => {
    const d = data.toString().trim()
    if (setPage(d)) return
    if (d === '') return
    if (d === ']') return
    if (d === ')') return
    if (d.match(/Latexmk: All targets .* are up-to-date/)) return
    let color = s => s
    if (d.includes('! ')) color = chalk.red
    if (d.startsWith('Overfull')) color = chalk.yellow
    if (d.startsWith('LaTeX Warning')) color = chalk.yellow
    if (d.includes('Warning:')) color = chalk.yellow
    if (d.includes('Underfull')) color = chalk.cyan
    for (const [re, c] of output) {
      if (d.match(re)) {
        if (c === 'suppress') return
        color = chalk[c]
        if (!color) {
          console.error(`color ${c} unknown`)
          color = chalk.white
        }
      }
    }
    console.log(ps(color(d)))
  })
  proc.stderr.on('data', data => {
    const d = data.toString().trim()
    if (setPage(d)) return
    if (d === '') return
    if (d === ']') return
    if (d === ')') return
    if (d.startsWith('Rc files read:\n')) return
    if (d.startsWith('Latexmk: Found input bbl file')) return
    if (d.startsWith('Latexmk: Found biber source file(s)')) return
    if (d.startsWith('Latexmk: Log file says output to')) return
    if (d.match(/Latexmk: Index file '.*' was written/)) return
    let color = chalk.red
    for (const [re, c] of output) {
      if (d.match(re)) {
        if (c === 'suppress') return
        color = chalk[c]
        if (!color) {
          console.error(`color ${c} unknown`)
          color = chalk.red
        }
      }
    }
    console.error(ps(color(d)))
  })

  return new Promise(resolve => proc.on('exit', resolve))
}

export default (config) => {
  config = Object.assign({
    plugins: [],
    indices: [],
    resources: [],
    papersize: 'statementpaper',
    output: []
  }, config)
  switch (config.papersize) {
    case 'letterpaper':
      config = Object.assign({
        margin: {
          spine: '.8in',
          edge: '.8in',
          upper: '.8in',
          lower: '.8in'
        },
        fontsize: '12pt'
      }, config)
      break
    case 'a4paper':
      config = Object.assign({
        margin: {
          spine: '20mm',
          edge: '20mm',
          upper: '20mm',
          lower: '20mm'
        },
        fontsize: '12pt'
      }, config)
      break
    case 'a5paper':
      // http://publishme.co.nz/wp/wp-content/uploads/PublishMe_standard_book_sizes_inside_page_specs.pdf
      // a5, outer=18mm, inner=25mm, top=20mm, bottom=27mm block=105mmx163mm
      config = Object.assign({
        margin: {
          spine: '25mm',
          edge: '18mm',
          upper: '20mm',
          lower: '27mm'
        },
        fontsize: '11pt'
      }, config)
      break
    case 'statementpaper':
      config = Object.assign({
        margin: {
          spine: '.9in',
          edge: '.7in',
          upper: '.8in',
          lower: '1in'
        },
        fontsize: '11pt'
      }, config)
      break
    case 'pottvopaper':
      config = Object.assign({
        margin: {
          spine: '.5in',
          edge: '.5in',
          upper: '.5in',
          lower: '.5in'
        },
        fontsize: '11pt'
      }, config)
      break
    default:
      console.log(chalk.red('Defaulting to 1in margins and 10pt font. ' +
        'This is not ideal!'))
      config = Object.assign({
        margin: {
          spine: '1in',
          edge: '1in',
          upper: '1in',
          lower: '1in'
        },
        fontsize: '10pt'
      }, config)
  }
  const lineMaps = {}
  const roots = {}
  return {
    fileExt: 'tex',
    remarkFilePlugin (opts) {
      this.Compiler = compiler

      const lineMap = {}
      const ctx = {
        lineMap,
        output: ''
      }
      function compiler (root, file) {
        lineMaps[file.history[0]] = lineMap
        roots[file.history[0]] = root
        ctx.file = file
        for (const p of config.plugins) {
          // TODO: Import these if string
          const m = p()
          m.handleRoot(root, ctx)
        }
        handleNode(root, ctx)
        return ctx.output
      }
    },
    async buildAfterFiles (bookConfig) {
      const { chapters, appendices, dedication } = bookConfig
      const files = [
        ...chapters,
        ...appendices,
        dedication
      ]
        .filter(c => !!c)
      await runChktex(files)
      await linkResources(bookConfig, config)
      await writeMain(bookConfig, config)
      const code = await runLatex(config.output)
      if (code) return false

      await pagenumbers.process(roots)
      if (config.outfile) {
        try {
          await fs.copyFile('main.pdf', config.outfile)
        } catch (e) {
          if (e.code !== 'ENOENT') throw e
        }
      }
      return !code
    }
  }
}
